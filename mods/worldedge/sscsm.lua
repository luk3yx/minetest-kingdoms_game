--
-- worldedge SSCSM
--
-- Copyright © 2019 by luk3yx.
-- License: https://luk3yx.mit-license.org/@2019
--

worldedge = {}
worldedge.edge = math.huge
local edge = math.huge

sscsm.register_on_com_receive('worldedge:edge', function(msg)
    assert(type(msg) == 'number')
    edge = msg
    worldedge.edge = msg
end)

function worldedge.outside_edge(pos)
    return pos.x > edge or pos.x < -edge or pos.z > edge or pos.z < -edge
end

function worldedge.reprimand_player(msg)
    minetest.display_chat_message(msg or
        'You cannot interact outside the world edge!')
    local player = minetest.localplayer
    if player and worldedge.outside_edge(player:get_pos()) then
        sscsm.com_send('worldedge:edge', 'reprimand')
    end
end

-- Prevent node interaction outside the world edge
local warned = false
minetest.register_on_punchnode(function(pos, node)
    if worldedge.outside_edge(pos) then
        if not warned then
            warned = true
            worldedge.reprimand_player()
            minetest.after(5, function() warned = false end)
        end
        return true
    end
end)

sscsm.com_send('worldedge:edge', 'get')
