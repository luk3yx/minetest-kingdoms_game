--
-- World edge
--
-- © Copyright 2019 by luk3yx
--

worldedge = {}

local edge = tonumber(minetest.settings:get('worldedge.edge'))
if not edge or edge ~= edge then
    edge = 2000
end

-- Teleport players back
local function outside_edge(pos)
    return pos.x > edge or pos.x < -edge or pos.z > edge or pos.z < -edge
end

local last_positions = {}
local function check_player(player)
    if not player then return end
    local name = player:get_player_name()
    local pos = player:get_pos()
    if outside_edge(pos) then
        local last_pos = last_positions[name]
        if not last_pos then
            last_pos = {
                x = math.max(math.min(pos.x, edge), -edge),
                y = pos.y,
                z = math.max(math.min(pos.z, edge), -edge),
            }
        end
        player:set_pos(last_pos)
        minetest.chat_send_player(name,
            'You cannot walk outside the world edge!')
    else
        last_positions[name] = pos
    end
end

minetest.register_on_joinplayer(check_player)
minetest.register_on_leaveplayer(function(player)
    last_positions[player:get_player_name()] = nil
end)

local function is_player(player)
    return player and player:get_player_name() ~= '' and
        not player.is_fake_player
end

-- Every 5 seconds store player positions
local function poll()
    for _, player in ipairs(minetest.get_connected_players()) do
        check_player(player)
    end
    minetest.after(5, poll)
end
minetest.after(5, poll)

-- API
worldedge.edge = edge

function worldedge.set_worldedge(new_edge)
    edge = new_edge
    worldedge.edge = new_edge
end

worldedge.outside_edge = outside_edge

function worldedge.reprimand_player(victim, msg)
    assert(minetest.is_player(victim))
    if not is_player(victim) then return end
    if msg ~= '' then
        minetest.chat_send_player(victim:get_player_name(),
            msg or 'You cannot interact outside the world edge!')
    end
    check_player(victim)
end

-- Mark everything outside the world edge as "protected" to prevent
-- digging/placing
local old_is_protected = minetest.is_protected
function minetest.is_protected(pos, name)
    if outside_edge(pos) then
        return true
    end
    return old_is_protected(pos, name)
end

minetest.register_on_protection_violation(function(pos, name)
    if not outside_edge(pos) then
        return
    end
    local player = minetest.get_player_by_name(name)
    if player then
        worldedge.reprimand_player(player)
    end
end)

if minetest.global_exists('sscsm') and sscsm.com_send then
    sscsm.register({
        name = 'worldedge',
        file = minetest.get_modpath('worldedge') .. '/sscsm.lua',
    })

    sscsm.register_on_com_receive('worldedge:edge', function(name, msg)
        if msg == 'get' then
            sscsm.com_send(name, 'worldedge:edge', edge)
        elseif msg == 'reprimand' then
            check_player(minetest.get_player_by_name(name))
        end
    end)

    local set_worldedge = worldedge.set_worldedge
    function worldedge.set_worldedge(new_edge)
        set_worldedge(new_edge)
        sscsm.com_send_all('worldedge:edge', new_edge)
    end
end
